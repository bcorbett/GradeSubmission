<?php
if(isset($_COOKIE["logged"]) && $_COOKIE["logged"]) {
  require_once('support.php');
  session_start();
  require_once('class.php');
  $numOfStudents = $_SESSION['numStudents'];
  $students = array();
  for ($x = 0; $x < $numOfStudents; $x++) {
    $students[$x] = new Student($_SESSION["Student".$x], $_POST["grade".$x]);
  }
  $_SESSION['studentInfo'] = serialize($students);
  $title = "Grades Submission";
  $body = <<<EOBODY
    <form action="finished.php" method="post">
      <h1>Grades to Submit</h1>
      <table border="5" cellpadding="5">
        <tr>
          <th>Name</th>
          <th>Grade</th>
        </tr>
EOBODY;
  for ($i = 0; $i < $numOfStudents; $i++) {
    $body .= "<tr><td>";
    $body .= $students[$i]->getName();
    $body .= "</td><td>";
    $body .= $students[$i]->getGrade();
    $body .="</td></tr>";
  }
  $body .= <<<EOBODY
      </table>
      <br/>
      <input type="submit" value="Submit Grades"/>
    </form>
    <br/>
    <form action="form.php">
      <input type="submit" value="Back"/>
    </form>
EOBODY;
  echo createSite($body, $title);
} else {
  die("<h1>Not logged in.</h1>");
}
?>