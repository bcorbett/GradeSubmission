<?php
session_start();
if(isset($_COOKIE["logged"]) && $_COOKIE["logged"]) {
  require_once('support.php');
  $title = "Grades Submission";
  $body = <<<EOBODY
    <h1>Grades submitted and e-mail confirmation sent.</h1>
    <h1>This is submission &num;1</h1>
EOBODY;
session_destroy();
  echo createSite($body, $title);
} else {
  die("<h1>Not logged in.</h1>");
}
?>