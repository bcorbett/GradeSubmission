<?php
require_once('support.php');
session_start();
if(isset($_COOKIE["logged"]) && $_COOKIE["logged"]){
  $title = "Grades Submission";
  $body = <<<EOBODY
  <form action="form.php" method="post">
  <h1>Section Information</h1>
  <strong>Course Name (e.g., cmsc128):</strong> <input type="text" name="course"/>
  <br/><br/>
  <strong>Section (e.g., 0101):</strong> <input type="text" name="section"/>
  <br/><br/>
  <input type="submit" value="Submit"/>
  </form>
EOBODY;
  echo createSite($body, $title);
  }else{
    die("<h1>Not logged in.</h1>");
  }
?>