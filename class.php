<?php
class Student{
  private $name;
  private $grade;
  public function __construct($studentName, $studentGrade){
    $this->name = $studentName;	      	    
    $this->grade = $studentGrade;
  }
  public function getName(){
    return $this->name;
  }
  public function getGrade(){
    return $this->grade;
  }
}
?>