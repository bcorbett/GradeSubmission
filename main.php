<?php
    require_once("support.php");
    session_start();
    if(isset($_COOKIE["logged"]) && $_COOKIE["logged"]){
        echo "<h1>You've logged in already. You'll be redirected to the page </h1>";
        header("refresh:5; url=section.php");
    }else{
        $body = "";
        if(isset($_POST["login"])){
            $login = trim($_POST["id"]);
            $passwordCode = trim($_POST["password"]);
            if(!($login === "cmsc298s" && $passwordCode === "terps")){
                $body .= "<br/><h1>Invalid login information provided.</h1><br/>";
            }
            if($body == ""){
                $_SESSION[$login] = $passwordCode;
                setcookie("logged", TRUE);
                header("Location: section.php");
            }
        }else{
            $login = "";
            $passwordCode = "";
        }
        $script = $_SERVER["PHP_SELF"];
        $head = <<<EOBODY
        <h1>Grades Submission System</h1>
        <form action="$script" method="post">
    <strong>LoginId:</strong> <input type="text" name="id" value="$login"/>
        <br/><br/>
    <strong>Password:</strong> <input type="password" name="password" value="$passwordCode"/>
        <br/><br/>
        <input type="submit" name="login" value="Submit"/>
        </form>
EOBODY;
        $body = $head.$body;
        $page = createSite($body);
        echo $page;
    }
    ?>
